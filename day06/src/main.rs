use adventofcode::*;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let mut school = input.parse::<School>()?;
    school.tick_n(80);
    answer!(1, school.len());

    // // this takes too long
    // school.tick_n(256-80);
    // answer!(2, school.len());

    let mut ocean = input.parse::<Ocean>()?;
    ocean.tick_n(256);
    answer!(2, ocean.len());

    Ok(())
}

const RESET_BABY: u8 = 8;
const RESET_ADULT: u8 = 6;

#[derive(Debug)]
struct LanternFish {
    timer: u8,
}

impl LanternFish {
    fn new(timer: u8) -> Self {
        LanternFish { timer }
    }

    fn tick(&mut self) -> Option<Self> {
        if self.timer == 0 {
            self.timer = RESET_ADULT;
            Some(LanternFish::new(RESET_BABY))
        } else {
            self.timer -= 1;
            None
        }
    }
}

#[derive(Debug)]
struct Ocean {
    fish: [usize; 9],
    days: usize,
}

impl Ocean {
    fn new() -> Self {
        Ocean {
            fish: [0; 9],
            days: 0,
        }
    }

    fn len(&self) -> usize {
        self.fish.iter().sum()
    }

    fn tick(&mut self) {
        let old = self.fish;

        for i in 0..9 {
            match i {
                0 => {
                    self.fish[8] = old[0];
                    self.fish[0] = old[1];
                }
                x => {
                    self.fish[x-1] = old[x];
                }
            }
        }

        self.fish[6] += old[0];
        self.days += 1;
    }
    
    fn tick_n(&mut self, n: usize) {
        for _ in 0..n {
            self.tick();
        }
    }
}

#[derive(Debug)]
struct School {
    fish: Vec<LanternFish>,
    days: usize,
}

impl School {
    fn tick(&mut self) {
        let mut babies = Vec::new();

        for fish in self.fish.iter_mut() {
            if let Some(baby) = fish.tick() {
                babies.push(baby);
            }
        }

        self.fish.append(&mut babies);
        self.days += 1;
    }

    fn tick_n(&mut self, n: usize) {
        for _ in 0..n {
            self.tick();
        }
    }

    fn len(&self) -> usize {
        self.fish.len()
    }
}

impl FromStr for LanternFish {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(LanternFish {
            timer: s.trim().parse()?
        })
    }
}

impl FromStr for School {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(School {
            fish: s.trim()
                .split(',')
                .map(|fish| fish.parse().unwrap())
                .collect(),
            days: 0,
        })
    }
}

impl FromStr for Ocean {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut ocean = Ocean::new();

        for timer in s.trim().split(',').map(|timer| timer.parse::<usize>().unwrap()) {
            ocean.fish[timer] += 1;
        }

        Ok(ocean)
    }
}
