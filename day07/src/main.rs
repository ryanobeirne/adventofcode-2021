use std::collections::HashMap;

use adventofcode::*;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let crabs = input.parse::<Crabs>()?;
    answer!(1, crabs.find_cheapest_fuel_pos().1);
    
    Ok(())
}

struct Crabs {
    positions: Vec<i32>,
}

impl Crabs {
    /// Align all crabs to position `pos` and return the fuel cost
    fn align(&self, pos: i32) -> usize {
        self.positions.iter()
            .map(|p| (pos - p).abs() as usize)
            .sum()
    }

    fn find_cheapest_fuel_pos(&self) -> (i32, usize) {
        let mut counter = HashMap::new();

        for pos in self.positions.iter() {
            counter.insert(*pos, self.align(*pos));
        }

        counter.into_iter().min_by(|(_, fuel_a), (_, fuel_b)| fuel_a.cmp(&fuel_b)).unwrap()
    }
}

impl FromStr for Crabs {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(Crabs {
            positions: s.trim().split(',').map(|i| i.parse::<i32>().unwrap()).collect(),
        })
    }
}
