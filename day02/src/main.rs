use adventofcode::*;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let mut pos_y: i32 = 0;
    let mut pos_z: i32 = 0;
    for command in input.lines().filter_map(|line| line.parse::<Command>().ok()) {
        match command {
            Command { direction: Forward, distance } => pos_y += distance,
            Command { direction: Up, distance } => pos_z -= distance,
            Command { direction: Down, distance } => pos_z += distance,
        }
    }

    answer!(1, pos_y * pos_z);

    let mut pos_y: i32 = 0;
    let mut depth: i32 = 0;
    let mut aim: i32 = 0;
    for command in input.lines().filter_map(|line| line.parse::<Command>().ok()) {
        match command {
            Command { direction: Forward, distance } => { pos_y += distance; depth += aim * distance },
            Command { direction: Up, distance } => aim -= distance,
            Command { direction: Down, distance } => aim += distance,
        }
    }

    answer!(2, pos_y * depth);

    Ok(())
}

struct Command {
    direction: Direction,
    distance: i32,
}

impl FromStr for Command {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.split_whitespace();
        Ok(
            Command {
                direction: split.next().ok_or("no direction")?.parse()?,
                distance: split.next().ok_or("no distance")?.parse()?,
            }
        )
    }
}

use Direction::*;
enum Direction {
    Forward,
    Down,
    Up,
}

impl FromStr for Direction {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(match s.trim() {
            "forward" => Forward,
            "down" => Down,
            "up" => Up,
            _ => return err!("invalid direction"),
        })
    }
}
