use adventofcode::*;
use std::collections::BTreeSet;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let mut bingo = input.parse::<Bingo>()?;

    answer!(1, bingo.first_score().unwrap());

    Ok(())
}

#[derive(Debug)]
struct Drawing {
    numbers: Vec<u8>,
}

impl FromStr for Drawing {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(
            Drawing {
                numbers: s.split(',').filter_map(|i| i.parse().ok()).collect(),
            }
        )
    }
}

#[derive(Debug)]    
struct Board {
    numbers: [u8; 25],
    match_indices: BTreeSet<usize>,
}

impl Board {
    fn new() -> Self {
        Board {
            numbers: [0; 25],
            match_indices: BTreeSet::new(),
        }
    }

    fn insert(&mut self, index: usize, element: u8) {
        self.numbers[index] = element;
    }

    fn call(&mut self, number: u8) -> Option<u32> {
        if let Some(index) = self.numbers.iter().position(|num| num == &number) {
            self.match_indices.insert(index);
            if self.has_bingo() {
                return Some(self.score());
            }
        }
        
        None
    }

    fn has_bingo(&self) -> bool {
        WINNING_INDICES.iter().any(|win| {
            win.iter().all(|idx| self.match_indices.contains(idx))
        })
    }

    fn score(&self) -> u32 {
        self.numbers.iter().enumerate()
            .filter(|(i, _num)| !self.match_indices.contains(i))
            .map(|(_i, n)| *n as u32)
            .sum()
    }
}

#[test]
fn has_bingo() {
    let mut bingo = Bingo {
        drawing: Drawing { numbers: vec![4,5,2,3,6] },
        call_index: 0,
        boards: vec![Board {
            numbers: [
                2, 3, 4, 5, 6,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
            ],
            match_indices: BTreeSet::new(),
        }],
    };

    bingo.call_next();
    bingo.call_next();
    bingo.call_next();
    bingo.call_next();
    assert!(!bingo.boards[0].has_bingo());

    bingo.call_next();
    assert!(bingo.boards[0].has_bingo());

    bingo.call_next();
    assert!(bingo.boards[0].has_bingo());
}

#[derive(Debug)]    
struct Bingo {
    drawing: Drawing,
    boards: Vec<Board>,
    call_index: usize,
}

impl Bingo {
    fn new(drawing: Drawing) -> Self {
        Bingo {
            drawing,
            boards: Vec::new(),
            call_index: 0,
        }
    }

    fn current_num(&self) -> u8 {
        self.drawing.numbers[self.call_index]
    }

    fn call_next(&mut self) -> Option<u32> {
        let current = self.current_num();
        for board in self.boards.iter_mut() {
            if let Some(score) = board.call(current) {
                return Some(score);
            }
        }
        self.call_index += 1;
        None
    }

    fn first_score(&mut self) -> Option<u32> {
        self.call_index = 0;
        for _i in 0..self.drawing.numbers.len() {
            for board in self.boards.iter() {
                if board.has_bingo() {
                    return Some(board.score() * self.current_num() as u32);
                }
            }
            self.call_next();
        }

        None
    }
}

impl FromStr for Bingo {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut lines = s.lines();

        let drawing = lines.next()
            .ok_or("empty first line")?
            .parse::<Drawing>()?;

        let mut bingo = Bingo::new(drawing);
        
        let join = lines
            .map(|line| format!("{}\n", line))
            .collect::<String>();

        let mut nums = join
            .split_whitespace()
            .map(|i| i.parse::<u8>().unwrap())
            .collect::<Vec<_>>();

        for i in 0..nums.len() {
            if i % 25 == 0 {
                bingo.boards.push(Board::new());
            }
            bingo.boards.last_mut().unwrap().insert(i % 25, nums.remove(0));
        }

        Ok(bingo)
    }
}

const WINNING_INDICES: &[[usize; 5]] = &[
    // Horizontal
    [0,   1,  2,  3,  4],
    [5,   6,  7,  8,  9],
    [10, 11, 12, 13, 14],
    [15, 16, 17, 18, 19],
    [20, 21, 22, 23, 24],

    // Vertical
    [0, 5, 10, 15, 20],
    [1, 6, 11, 16, 21],
    [2, 7, 12, 17, 22],
    [3, 8, 13, 18, 23],
    [4, 9, 14, 19, 24],

    // Diagonal
    // [0, 6, 12, 18, 24],
    // [4, 8, 12, 16, 20],
];

#[test]
fn example_1() -> Result<()> {
    let mut input = String::new();
    input!("example").read_to_string(&mut input)?;

    let mut bingo = input.parse::<Bingo>()?;
    dbg!(&bingo);
    assert_eq!(bingo.first_score().unwrap(), 4512);

    Ok(())
}
