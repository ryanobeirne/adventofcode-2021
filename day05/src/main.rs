use std::collections::{BTreeSet, HashMap};
use std::cmp::Ordering::*;

use adventofcode::*;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let vents = input.parse::<Vents>()?;
    answer!(1, vents.overlaps_ortho().len());
    answer!(2, vents.overlaps_all().len());

    Ok(())
}

#[derive(Debug)]
struct Lines {
    inner: Vec<Line>,
}

impl Lines {
    fn split_ortho(self) -> Vents {
        let mut horizontal = Vec::new();
        let mut vertical = Vec::new();
        let mut diagonal = Vec::new();

        for line in self.inner.into_iter() {
            if line.is_horizontal() {
                horizontal.push(line);
            } else if line.is_vertical() {
                vertical.push(line);
            } else {
                diagonal.push(line);
            }
        }

        Vents {
            horizontal: Lines {inner: horizontal},
            vertical: Lines {inner: vertical},
            diagonal: Lines {inner: diagonal},
        }
    }

    /// Points in the chart occupied by one or more lines
    fn line_points(&self) -> Points {
        self.inner.iter().flat_map(|line| line.points()).collect()
    }
}

#[derive(Debug)]
struct Line {
    a: Point,
    b: Point,
}

impl Line {
    fn is_horizontal(&self) -> bool {
        self.a.y == self.b.y
    }

    fn is_vertical(&self) -> bool {
        self.a.x == self.b.x
    }

    fn is_diagonal(&self) -> bool {
        (self.a.x as i32 - self.b.x as i32).abs() == (self.a.y as i32 - self.b.y as i32).abs()
    }
    fn points(&self) -> Points {
        let mut inner = Vec::new();

        let mut point = self.a;
        while point != self.b {
            inner.push(point);
            point = point.go(self.direction());
        }

        inner.push(point);

        Points {
            inner,
            index: 0,
        }
    }

    fn direction(&self) -> Direction {
        match (self.a.x.cmp(&self.b.x), self.a.y.cmp(&self.b.y)) {
            (Less, diff_y) => match diff_y {
                Less => SouthEast,
                Equal => East,
                Greater => NorthEast,
            },

            // Good
            (Equal, diff_y) => match diff_y {
                Less => South,
                Equal => Stay,
                Greater => North,
            }

            (Greater, diff_y) => match diff_y {
                Less => SouthWest,
                Equal => West,
                Greater => NorthWest,
            }
        }
    }
}

use Direction::*;
enum Direction {
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
    West,
    NorthWest,
    Stay,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Point {
    x: u32,
    y: u32,
}

impl Point {
    fn new(x: u32, y: u32) -> Self {
        Point {x, y}
    }

    fn go(&self, direction: Direction) -> Point {
        match direction {
            North     => Point::new(self.x,     self.y - 1), // Good
            NorthEast => Point::new(self.x + 1, self.y - 1), // Good
            East      => Point::new(self.x + 1, self.y    ), // Good
            SouthEast => Point::new(self.x + 1, self.y + 1), // Good
            South     => Point::new(self.x,     self.y + 1), // Good
            SouthWest => Point::new(self.x - 1, self.y + 1), // Good
            West      => Point::new(self.x - 1, self.y    ), // Good
            NorthWest => Point::new(self.x - 1, self.y - 1), // Good
            Stay      => Point::new(self.x,     self.y    ), // Good
        }
    }
}


#[derive(Debug)]
struct Points {
    inner: Vec<Point>,
    index: usize,
}

impl Iterator for Points {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.inner.len() {
            None
        } else {
            self.index += 1;
            Some(self.inner[self.index - 1])
        }
    }
}

impl FromIterator<Point> for Points {
    fn from_iter<T: IntoIterator<Item = Point>>(iter: T) -> Self {
        Points {
            inner: iter.into_iter().collect(),
            index: 0,
        }
    }
}

impl FromStr for Lines {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(
            Lines {
                inner: s.trim().lines().map(|line| line.parse().unwrap()).collect(),
            }
        )
    }
}

impl FromStr for Line {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.trim().split(" -> ");
        Ok(
            Line {
                a: split.next().unwrap_or("a value not found").parse()?,
                b: split.next().unwrap_or("b value not found").parse()?,
            }
        )
    }
}

impl FromStr for Point {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut split = s.trim().split(',');
        Ok(
            Point {
                x: split.next().unwrap_or("x value not found").parse()?,
                y: split.next().unwrap_or("y value not found").parse()?,
            }
        )
    }
}

#[test]
fn points() -> Result<()> {
    // Vertical
    let line = Line { a: Point::new(1, 1), b: Point::new(1, 3) };
    assert!(line.is_vertical());

    let points = line.points();
    assert_eq!(points.inner[0], Point::new(1, 1));
    assert_eq!(points.inner[1], Point::new(1, 2));
    assert_eq!(points.inner[2], Point::new(1, 3));

    // Horizontal
    let line = Line { a: Point::new(9, 7), b: Point::new(7, 7) };
    assert!(line.is_horizontal());

    let points = line.points();
    assert_eq!(points.inner[0], Point::new(9, 7));
    assert_eq!(points.inner[1], Point::new(8, 7));
    assert_eq!(points.inner[2], Point::new(7, 7));

    // Diagonal
    let line = Line { a: Point::new(9, 7), b: Point::new(7, 9) };
    assert!(line.is_diagonal());

    let points = line.points();
    assert_eq!(points.inner[0], Point::new(9, 7));
    assert_eq!(points.inner[1], Point::new(8, 8));
    assert_eq!(points.inner[2], Point::new(7, 9));

    Ok(())
}

#[test]
fn example1() -> Result<()> {
    let mut input = String::new();
    input!("example").read_to_string(&mut input)?;

    let vents = input.parse::<Vents>()?;
    let overlaps = vents.overlaps_ortho();
    assert_eq!(overlaps.len(), 5);

    Ok(())
}

#[test]
fn example2() -> Result<()> {
    let mut input = String::new();
    input!("example").read_to_string(&mut input)?;

    let vents = input.parse::<Vents>()?;
    let overlaps = vents.overlaps_all();
    assert_eq!(overlaps.len(), 12);

    Ok(())
}

struct Vents {
    horizontal: Lines,
    vertical: Lines,
    diagonal: Lines,
}

impl FromStr for Vents {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(s.parse::<Lines>()?.split_ortho())
    }
}

impl Vents {
    /// Points in the chart occupied by more than one line
    fn overlaps_ortho(&self) -> BTreeSet<Point> {
        assert!(self.horizontal.inner.iter().all(Line::is_horizontal), "self lines are not horizontal");
        assert!(self.vertical.inner.iter().all(Line::is_vertical), "other lines are not vertical");

        let mut counter = HashMap::new();
        for point in self.horizontal.line_points().chain(self.vertical.line_points()) {
            *counter.entry(point).or_insert(0) += 1;
        }

        counter.into_iter()
            .filter(|(_point, count)| count > &1)
            .map(|(point, _count)| point)
            .collect()
    }

    fn overlaps_all(&self) -> BTreeSet<Point> {
        assert!(self.horizontal.inner.iter().all(Line::is_horizontal), "lines are not horizontal");
        assert!(self.vertical.inner.iter().all(Line::is_vertical), "lines are not vertical");
        assert!(self.diagonal.inner.iter().all(Line::is_diagonal), "lines are not diagonal");

        let mut counter = HashMap::new();
        for point in self.horizontal
            .line_points()
            .chain(self.vertical.line_points())
            .chain(self.diagonal.line_points()) {
            *counter.entry(point).or_insert(0) += 1;
        }

        counter.into_iter()
            .filter(|(_point, count)| count > &1)
            .map(|(point, _count)| point)
            .collect()
    }
}
