use std::{collections::{BTreeMap}, fmt::{Debug, Display}, iter::FromIterator};
use adventofcode::*;

#[derive(Copy, Clone, PartialEq, Eq)]
struct Bits12 {
    bits: [bool; 12],
}

impl Bits12 {
    fn flipped(&self) -> Self {
        let mut bits = self.bits;
        for bit in bits.iter_mut() {
            *bit = !*bit;
        }
        Bits12 { bits }
    }

    fn as_usize(&self) -> usize {
        usize::from(*self)
    }

    #[cfg(test)]
    fn with_n_bits(mut self, n_bits: usize) -> Self {
        for i in 0..(12-n_bits) {
            self.bits[i] = false;
        }
        self
    }
}

impl Display for Bits12 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for bit in self.bits.iter() {
            f.write_str(&(*bit as u8).to_string())?;
        }
        Ok(())
    }
}

impl Debug for Bits12 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("Bits12[{}]", self))?;
        Ok(())
    }
}

impl FromStr for Bits12 {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        let mut bits = [false; 12];
        let mut chars = s.chars().rev();

        for i in (0..12).rev() {
            if let Some(c) = chars.next() {
                match c {
                    '1' => bits[i] = true,
                    '0' => (),
                    d => return err!("invalid binary digit: `{}`", d),
                }
            }
        }

        Ok(Bits12 { bits })
    }
}

impl From<Bits12> for usize {
    fn from(bits12: Bits12) -> Self {
        let mut int = 0;

        for (i, bits_pos) in (0..12).rev().enumerate() {
            if bits12.bits[bits_pos] {
                int += 1 << i;
            }
        }

        int
    }
}

impl From<usize> for Bits12 {
    fn from(int: usize) -> Self {
        let mut bits = [false; 12];
        for (i, shift) in (0..12).rev().enumerate() {
            match int >> shift & 1 {
                1 => bits[i] = true,
                0 => (),
                _ => unreachable!(),
            }
        }
        Bits12 { bits }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Nums {
    nums: Vec<Bits12>,
}

impl Nums {
    fn gamma(&self) -> Bits12 {
        let mut map = BTreeMap::new();

        for bits12 in self.nums.iter() {
            for i in 0..12 {
                let entry = map.entry(i).or_insert(0);
                if bits12.bits[i] {
                    *entry += 1;
                }
            }
        }

        let mut bits = [false; 12];
        let len = self.nums.len();

        for i in 0..12 {
            if let Some(ones) = map.get(&i) {
                let zeros = len - ones;
                if &zeros <= ones {
                    bits[i] = true;
                }
            }
        }

        Bits12 { bits }
    }

    fn epsilon(&self) -> Bits12 {
        self.gamma().flipped()
    }

    fn power(&self) -> usize {
        self.gamma().as_usize() * self.epsilon().as_usize()
    }

    #[cfg(test)]
    fn power_with_n_bits(&self, n_bits: usize) -> usize {
        self.gamma().with_n_bits(n_bits).as_usize() * self.epsilon().with_n_bits(n_bits).as_usize()
    }

    fn most_common_pos(&self, pos: usize) -> bool {
        let mut map = BTreeMap::new();

        for num in self.nums.iter() {
            *map.entry(num.bits[pos]).or_insert(0) += 1;
        }

        let zeros = map.get(&false).unwrap_or(&0);
        let ones = map.get(&true).unwrap_or(&0);
        
        if zeros <= ones {
            true
        } else {
            false
        }
    }

    fn least_common_pos(&self, pos: usize) -> bool {
        let mut map = BTreeMap::new();

        for num in self.nums.iter() {
            *map.entry(num.bits[pos]).or_insert(0) += 1;
        }

        let zeros = map.get(&false).unwrap_or(&0);
        let ones = map.get(&true).unwrap_or(&0);
        
        if zeros <= ones {
            false
        } else {
            true
        }
    }

    fn o2gen(&self, n_bits: usize) -> Bits12 {
        let mut nums = self.nums.iter().cloned().collect::<Nums>();

        for i in (12 - n_bits)..12 {
            let common = nums.most_common_pos(i);
            nums = nums.nums.into_iter().filter(|num| num.bits[i] == common).collect();
            if nums.nums.len() < 2 { break }
        }

        nums.nums[0]
    }

    fn co2scrub(&self, n_bits: usize) -> Bits12 {
        let mut nums = self.nums.iter().cloned().collect::<Nums>();

        for i in (12 - n_bits)..12 {
            let least_common = nums.least_common_pos(i);
            nums = nums.nums.into_iter().filter(|num| num.bits[i] == least_common).collect();
            if nums.nums.len() < 2 { break }
        }

        nums.nums[0]
    }

    fn life_support(&self) -> usize {
        self.o2gen(12).as_usize() * self.co2scrub(12).as_usize()
    }

    #[cfg(test)]
    fn life_support_with_n_bits(&self, n_bits: usize) -> usize {
        self.o2gen(n_bits).with_n_bits(n_bits).as_usize() * self.co2scrub(n_bits).with_n_bits(n_bits).as_usize()
    }
}

impl FromIterator<Bits12> for Nums {
    fn from_iter<T: IntoIterator<Item = Bits12>>(iter: T) -> Self {
        Nums {
            nums: iter.into_iter().collect(),
        }
    }
}

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let nums = input.lines().filter_map(|line| line.parse().ok()).collect::<Nums>();
    answer!(1, nums.power());
    answer!(2, nums.life_support());

    Ok(())
}

#[test]
fn flip_bits() {
    let test_bits: Bits12 = "000110".parse().unwrap();

    assert_eq!(
        test_bits,
        Bits12 {
            bits: [
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                true,
                false,
            ],
        }
    );

    assert_eq!(
        test_bits.flipped(),
        Bits12 {
            bits: [
                true,
                true,
                true,
                true,
                true,
                true,
                true,
                true,
                true,
                false,
                false,
                true,
            ],
        }
    );

}

#[cfg(test)]
const NUMS_EXAMPLE_1: [usize; 12] = [
    0b00100,
    0b11110,
    0b10110,
    0b10111,
    0b10101,
    0b01111,
    0b00111,
    0b11100,
    0b10000,
    0b11001,
    0b00010,
    0b01010,
];

#[test]
fn parse_nums() {
    let bits = Bits12::from_str("00100").unwrap();
    assert_eq!(bits.as_usize(), NUMS_EXAMPLE_1[0]);
    assert_eq!(Bits12::from(NUMS_EXAMPLE_1[0]), bits);

    for i in 0..NUMS_EXAMPLE_1.len() {
        let bits = Bits12::from(NUMS_EXAMPLE_1[i]);
        assert_eq!(
            bits,
            Bits12::from_str(&format!("{:012b}", NUMS_EXAMPLE_1[i])).unwrap()
        );
        assert_eq!(bits.as_usize(), NUMS_EXAMPLE_1[i]);
    }
}

#[test]
fn example_1() {
    let nums = NUMS_EXAMPLE_1.iter().map(|num| Bits12::from(*num)).collect::<Nums>();
    dbg!(&nums);
    assert_eq!(nums.gamma().with_n_bits(5).as_usize(), 22);
    assert_eq!(nums.epsilon().with_n_bits(5).as_usize(), 9);
    assert_eq!(nums.power_with_n_bits(5), 198);
}

#[test]
fn example_2() {
    let nums = NUMS_EXAMPLE_1.iter().map(|num| Bits12::from(*num)).collect::<Nums>();
    assert_eq!(nums.o2gen(5).with_n_bits(5).as_usize(), 23);
    assert_eq!(nums.co2scrub(5).with_n_bits(5).as_usize(), 10);
    assert_eq!(nums.life_support_with_n_bits(5), 230);
}
