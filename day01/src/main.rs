use adventofcode::*;

fn main() -> Result<()> {
    let mut input = String::new();
    input!(1).read_to_string(&mut input)?;

    let mut peekable = input.lines().peekable();

    let mut count = 0;
    while let Some(depth) = peekable.next() {
        if let Some(peek) = peekable.peek() {
            let depth = depth.parse::<usize>()?;
            let peek = peek.parse::<usize>()?;
            if peek > depth {
                count += 1;
            }
        }
    }

    answer!(1, count);

    let lines = input.lines().collect::<Vec<_>>();
    let mut windows = lines.windows(3).peekable();

    let mut count = 0;
    while let Some(depths) = windows.next() {
        if let Some(peek) = windows.peek() {
            let sum: usize = depths.iter()
                .filter_map(|depth| Some(depth.parse::<usize>().ok()?))
                .sum();
            let sum_next: usize = peek.iter()
                .filter_map(|depth| Some(depth.parse::<usize>().ok()?))
                .sum();
            if sum_next > sum {
                count += 1;
            }
        }
    }

    answer!(2, count);

    Ok(())
}


